Format: 3.0 (quilt)
Source: bgscripts
Binary: bgscripts, bgscripts-core
Architecture: all
Version: 1.5.2-1
Maintainer: B Stack <bgstack15@gmail.com>
Homepage: https://bgstack15.wordpress.com/
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 12~), txt2man
Package-List:
 bgscripts deb utils optional arch=all
 bgscripts-core deb utils optional arch=all
Files:
 00000000000000000000000000000000 1 bgscripts.orig.tar.gz
 00000000000000000000000000000000 1 bgscripts.debian.tar.xz
