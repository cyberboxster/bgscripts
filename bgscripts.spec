# File: bgscripts.spec
# Location: bgscripts tarball
# Author: bgstack15
# Startdate: 2016
# Title: Rpm Spec for Bgscripts Package
# Purpose: Provide build instructions for Fedora rpm for package
# History:
#    2016 start spec for bgscripts-1.0-1
# Usage:
# Reference:
#    https://fedoraproject.org/wiki/Packaging:Scriptlets
#    https://fedoraproject.org/wiki/Changes/systemd_file_triggers
#    https://superuser.com/questions/1017959/how-to-know-if-i-am-using-systemd-on-my-linux
#    rpmrebuild -e ntp
# Improve:
# Documentation:
# Dependencies:

%define devtty "/dev/null"
%define debug_package %{nil}
%global _python_bytecompile_errors_terminate_build 0

Summary:	   bgscripts gui components
Name:		   bgscripts
Version:	   1.5.2
Release:	   1
License:	   CC BY-SA 4.0
Group:      Applications/System
Source:     %{name}-%{version}.tgz
URL:        https://bgstack15.wordpress.com/
#Distribution:
#Vendor:
Packager:   B Stack <bgstack15@gmail.com>
Requires:   %{name}-core >= %{version}-%{release}
Requires:   procps-ng
BuildRequires: txt2man
%if 0%{?fedora} >= 34
BuildRequires: make
%endif
%if ! 0%{?el6}%{?el7}
Recommends: freerdp, zenity
%endif
Buildarch:	noarch
Provides:	application(rdp.desktop)
Provides:	mimehandler(application/x-rdp)

%package core
Summary:        bgscripts core components
BuildRequires:  coreutils
Obsoletes:      %{name} < 1.1-31
%if ! 0%{?el6}%{?el7}
Recommends:     %{name}, expect, bash-completion
%endif

%description core
bgscripts-core is is the cli components of the bgscripts suite.
Bgscripts provides helpful scripts that sysadmins could find useful.
The most important ones include:
bup ctee dli lecho newscript updateval framework dnskeepalive

Also included is "bp" which is bgscripts.bashrc

%description
bgscripts is the gui components of the bgscripts suite, including rdp.sh.

%prep
%setup -q -c %{name}

%build
export srcdir="$( find . -type d -name 'src' -printf '%%P' )"
%make_build -C "${srcdir}"

%install
export srcdir="$( find . -type d -name 'src' -printf '%%P' )"
%make_install -C "${srcdir}" with_sysd_monitor_resize=YES with_sysv_monitor_resize=NO
exit 0

%clean
rm -rf %{buildroot}

%preun
if test "$1" = "0";
then
   systemctl --no-reload disable --now monitor-resize.service 1>/dev/null 2>&1 || :
fi
exit 0

%postun
# upgrade package, not uninstall
if test "$1" -ge 1 ;
then
   systemctl try-restart monitor-resize.service 1>/dev/null 2>&1 || :
fi
exit 0

%post core
exit 0

%preun core
exit 0

%postun core
exit 0

%if 0%{?fedora} || 0%{?rhel} >= 7
%posttrans
touch --no-create %{_datadir}/icons/hicolor 1>/dev/null 2>&1 || :
gtk-update-icon-cache %{_datadir}/icons/hicolor 1>/dev/null 2>&1 || :
update-desktop-database 1>/dev/null 2>&1 || :
upate-mime-database %{_datadir}/mime 1>/dev/null 2>&1 || :

# for monitor-resize
systemctl daemon-reload 1>/dev/null 2>&1 || :
systemctl --no-reload preset monitor-resize.service 1>/dev/null 2>&1 || :
%endif

%files
%config %attr(666, -, -) %{_sysconfdir}/%{name}/monitor-resize.conf
%config %attr(666, -, -) %{_sysconfdir}/%{name}/rdp.conf
%{_sysconfdir}/sysconfig/*-*
%{_sysconfdir}/xdg/autostart
%{_bindir}/mp4tomp3
%{_bindir}/rdp
%{_bindir}/resize-x
%{_bindir}/screensize
%{_bindir}/xscreensaver-watch
%{_sbindir}/monitor-resize
%{_datadir}/applications
%{_datadir}/%{name}/icons
%{_datadir}/icons
%{_datadir}/mime
%{_mandir}/man1/mp4tomp3.1.gz
%{_mandir}/man1/rdp.1.gz
%{_mandir}/man1/resize-x.1.gz
%{_mandir}/man1/screensize.1.gz
%{_mandir}/man1/xscreensaver-watch.1.gz
%{_mandir}/man8/monitor-resize.8.gz
%attr(0644, -, -) %{?_unitdir}%{!?_unitdir:/usr/lib/systemd/system}/monitor-resize.service

%files core
%dir %{_sysconfdir}/%{name}
%{_sysconfdir}/%{name}/dnskeepalive.conf
%{_sysconfdir}/sysconfig/dnskeepalive
%attr(440, root, root) %{_sysconfdir}/sudoers.d/*
%dir %{_datadir}/%{name}
%{_bindir}/apt-summary
%{_bindir}/beep
%{_bindir}/bounce
%{_bindir}/bp
%{_bindir}/bup
%{_bindir}/ctee
%{_bindir}/dli
%{_bindir}/drillup
%{_bindir}/fl
%{_bindir}/getpwhash
%{_bindir}/host-bup
%{_bindir}/hwset
%{_bindir}/istruthy
%{_bindir}/kinit-host
%{_bindir}/lecho
%{_bindir}/learn-used-grub-entry
%{_bindir}/list-active-repos
%{_bindir}/make-dsc-for-obs
%{_bindir}/newscript
%{_bindir}/obs-dl
%{_bindir}/plecho
%{_bindir}/send
%{_bindir}/shares
%{_bindir}/sizer
%{_bindir}/sslscanner
%{_bindir}/title
%{_bindir}/toucher
%{_bindir}/txt2man-wrapper
%{_bindir}/updateval
%{_bindir}/url-encode
%{_libexecdir}/%{name}
%{_datadir}/%{name}/bashrc.d
%attr(755, -, -) %{_datadir}/%{name}/deprecated/*
%config %attr(664, -, -) %{_datadir}/%{name}/examples/*
%{_datadir}/%{name}/systemd
%{_datadir}/%{name}/sysvinit
%{_sbindir}/dnskeepalive
%if 0%{?rhel} <= 7
%doc %attr(444, -, -) %{_docdir}/%{name}/*
%else
%doc %attr(444, -, -) %{_pkgdocdir}/*
%endif
%{_mandir}/man1/apt-summary.1.gz
%{_mandir}/man1/beep.1.gz
%{_mandir}/man1/bounce.1.gz
%{_mandir}/man1/bp.1.gz
%{_mandir}/man1/bup.1.gz
%{_mandir}/man1/ctee.1.gz
%{_mandir}/man1/dli.1.gz
%{_mandir}/man1/drillup.1.gz
%{_mandir}/man1/fl.1.gz
%{_mandir}/man1/getpwhash.1.gz
%{_mandir}/man1/host-bup.1.gz
%{_mandir}/man1/hwset.1.gz
%{_mandir}/man1/istruthy.1.gz
%{_mandir}/man1/kinit-host.1.gz
%{_mandir}/man1/learn-used-grub-entry.1.gz
%{_mandir}/man1/lecho.1.gz
%{_mandir}/man1/list-active-repos.1.gz
%{_mandir}/man1/make-dsc-for-obs.1.gz
%{_mandir}/man1/newscript.1.gz
%{_mandir}/man1/obs-dl.1.gz
%{_mandir}/man1/plecho.1.gz
%{_mandir}/man1/send.1.gz
%{_mandir}/man1/shares.1.gz
%{_mandir}/man1/sizer.1.gz
%{_mandir}/man1/sslscanner.1.gz
%{_mandir}/man1/title.1.gz
%{_mandir}/man1/toucher.1.gz
%{_mandir}/man1/txt2man-wrapper.1.gz
%{_mandir}/man1/updateval.1.gz
%{_mandir}/man1/url-encode.1.gz
%{_mandir}/man8/dnskeepalive.8.gz
%{_mandir}/man5/*z
%{_mandir}/man7/*z

%changelog
* Thu Aug 13 2020 B Stack <bgstack15@gmail.com> - 1.5.2-1
- Version bump
