#!/bin/sh
# Filename: shldap.sh
# Locations: /usr/share/bgscripts/
# Author: bgstack15
# Startdate: 2018-11-06 15:03
# Title: Library for Shell Ldap Functions
# Purpose: To abstract out my shell code
# History:
# Usage:
# Reference:
#    Ripped the 4 original functions from lapsldap.sh
# Improve:
#    update_ldap does not have the ability to blank out or delete attributes
#    add a function for "get_a_dc" that does the dig trick:
#       dig +short -t srv _ldap._tcp.example.com | awk 'NR==1{gsub(".$","",$NF);print $NF}'
# Dependencies:
#    awk (gawk)
#    cp, echo, test (coreutils)
#    functions from framework.sh (bgscripts-core)
#    kinit, klist (krb5-workstation)
#    ldapsearch, ldapmodify (openldap-clients)
#    sed (sed)
#    xargs (findutils)
#    dep-raw: awk, cp, echo, test, framework.sh, kinit, klist, ldapsearch, ldapmodify, sed, xargs
#    rec-devuan: gawk | mawk, coreutils, sed
#    sug-devuan: krb5-user, ldap-utils
SHLDAP_VERSION="2019-07-17b"

get_host_keytab() {
   # call: get_host_keytab "${SHLDAP_KINIT_HOST_SCRIPT}" "${SHLDAP_KINIT_HOST_SCRIPT_OPTS}" "${SHLDAP_KLIST_BIN}" "${SHLDAP_KRB5CC_TMPFILE}"
   # returns: nothing.
   # action: get host kerberos ticket-granting ticket
   # dependencies: needs access to /etc/krb5.keytab, so probably need to run as root.
   debuglev 10 && ferror "get_host_keytab $@"
   ___ghk_kinit_host_script="${1}"
   ___ghk_kinit_host_script_opts="${2}"
   ___ghk_klist_bin="${3}"
   ___ghk_krb5cc_tmpfile="${4}"

   # test if can access host keytab
   if ! test -r "/etc/krb5.keytab" ; then ferror "${scriptfile}: cannot access host keytab: /etc/krb5.keytab. Are you running as root?" ; fi

   if test -e "${___ghk_kinit_host_script}" ;
   then
      KRB5CCNAME=FILE:"${___ghk_krb5cc_tmpfile}" "${___ghk_kinit_host_script}" ${___ghk_kinit_host_script_opts}
   else
      debuglev 3 && ferror "debug3: Using built-in logic to fetch host kerberos ticket because unable to find _KINIT_HOST_SCRIPT=${___ghk_kinit_host_script}"
      # do internal logic here
      # find kinit
      ___ghk_kinit_bin="$( find "${SHLDAP_KINIT_BIN}" /usr/bin/kinit /bin/kinit /usr/local/bin/kinit -print -quit 2>/dev/null | head -n1 )"
      if ! test -e "${___ghk_kinit_bin}" ;
      then
         ferror "${scriptname}: 4 fatal! Unable to find kinit. Please use the appropriate variable _KINIT_BIN. Aborted."
      fi
      # cannot use requested server name here. root@localhost can only use its own kerberos ticket.
      # observe that no domain name is given (after the dollar sign). This will force kerberos to choose, based on the default_realm value in /etc/krb5.conf.
      "${___ghk_kinit_bin}" -k -c "${___ghk_krb5cc_tmpfile}" "$( hostname -s | tr '[[:lower:]]' '[[:upper:]]' )\$" | debuglevoutput 7
   fi

   # return true if klist returns true
   "${___ghk_klist_bin}" -c "${___ghk_krb5cc_tmpfile}" | debuglevoutput 7

}

get_attrib_from_ldap() {
   # call: get_attrib_from_ldap "${LAPS_LDAPSEARCH_BIN}" "${LAPS_LDAPSEARCH_FLAGS}" "${LAPS_LDAPSEARCH_FILTER}" "${LAPS_ATTRIB_TIME}" "${LAPS_LDAPCONF}" "${LAPS_KRB5CC_TMPFILE}" "{LAPS_LDAPSEARCH_STATUS_TMPFILE}"
   debuglev 10 && ferror "get_attrib_from_ldap $@"
   ___gtfl_ldapsearch_bin="${1}"
   ___gtfl_ldapsearch_flags="${2}"
   ___gtfl_ldapsearch_filter="${3}"
   ___gtfl_attrib="${4}"
   ___gtfl_ldapconf="${5}"
   ___gtfl_krb5cc_tmpfile="${6}"
   ___gtfl_ldapsearch_status_tmpfile="${7}"

   # execute for the purpose of displaying when debug level is high enough
   {
      debuglev 8 && set -x
      KRB5CCNAME="${___gtfl_krb5cc_tmpfile}" LDAPCONF="${___gtfl_ldapconf}" "${___gtfl_ldapsearch_bin}" ${___gtfl_ldapsearch_flags} "${___gtfl_ldapsearch_filter}" "${___gtfl_attrib}" 2>&1 | debuglevoutput 8
      set +x
   } 1>&2

   # execute to check for ldap or kerberos errors
   ___gtfl_stderr="$( KRB5CCNAME="${___gtfl_krb5cc_tmpfile}" LDAPCONF="${___gtfl_ldapconf}" "${___gtfl_ldapsearch_bin}" ${___gtfl_ldapsearch_flags} "${___gtfl_ldapsearch_filter}" "${___gtfl_attrib}" 2>&1 1>/dev/null )"
   ___gtfl_stderr_response="$?"
   if test ${___gtfl_stderr_response} -ne 0 ;
   then
      if echo "${___gtfl_stderr}" | grep -qiE 'Ticket expired' ;
      then
         ferror "Fatal: Kerberos ticket expired."
         return "${___gtfl_stderr_response}"
      elif echo "${___gtfl_stderr}" | grep -qi -e 'SASL(-1): generic failure: GSSAPI Error: An invalid name was supplied (Success)' ;
      then
         ferror "Fatal: GSSAPI Error: Invalid name (Success). Try using \"SASL_NOCANON on\" in lapsldap.conf."
         return "${___gtfl_stderr_response}"
      elif echo "${___gtfl_stderr}" | grep -qi -e 'TLS: hostname does not match CN in peer certificate' ;
      then
         ferror "Fatal: TLS: hostname does not match CN. Try using \"TLS_REQCERT allow\" in lapsldap.conf."
         return "${___gtfl_stderr_response}"
      else
         {
            echo "Fatal: other ldap error:"
            echo "${___gtfl_stderr}"
         } | debuglevoutput 9
         return "${___gtfl_stderr_response}"
      fi
   fi

   # execute for actually fetching the value
   ___gtfl_attrib="$( { KRB5CCNAME="${___gtfl_krb5cc_tmpfile}" LDAPCONF="${___gtfl_ldapconf}" \
         "${___gtfl_ldapsearch_bin}" ${___gtfl_ldapsearch_flags} "${___gtfl_ldapsearch_filter}" \
         "${___gtfl_attrib}" 2>/dev/null ; \
         echo "$?" > "${___gtfl_ldapsearch_status_tmpfile}" ; \
      } | sed -r -e 's/^#.*$//;' -e '/^\s*$/d' | grep -iE -e "^${___gtfl_attrib}:" | sed -r -e "s/^${___gtfl_attrib}:\s*//;" )"
   ___gtfl_ldap_success="$( { cat "${___gtfl_ldapsearch_status_tmpfile}" 2>/dev/null ; echo "1" ; } | head -n1 )"
   if test "${___gtfl_ldap_success}" != "0" ;
   then
      ferror "Fatal: LDAP lookup failed"
      return 1
   fi

   # here we can be sure that an empty value means there was no attribute by
   # that name defined or it had an actual empty value.

   echo "${___gtfl_attrib}"

}

update_ldap() {
   # call: update_ldap "${LAPS_LDAPSEARCH_BIN}" "${LAPS_LDAPSEARCH_FLAGS}" "${LAPS_LDAPSEARCH_FILTER}" "${LAPS_LDAPSEARCH_UNIQUE_ID}" "${LAPS_LDAPCONF}" "${LAPS_KRB5CC_TMPFILE}" "|" "${LAPS_ATTRIB_PW}" "${LAPS_phrase}" "${LAPS_LDIF_TMPFILE}" "${LAPS_LDAPMODIFY_BIN}" "${LAPS_LDAPMODIFY_FLAGS}" "${LAPS_TEST}" "${LAPS_TMPFILE1}"
   debuglev 10 && ferror "update_ldap $@"
   ___ul_ldapsearch_bin="${1}"
   ___ul_ldapsearch_flags="${2}"
   ___ul_ldapsearch_filter="${3}"
   ___ul_ldapsearch_unique_id="${4}"
   ___ul_ldapconf="${5}"
   ___ul_krb5cc_tmpfile="${6}"
   ___ul_delimiter="${7}"
   ___ul_attribs="${8}"
   ___ul_values="${9}"
   ___ul_ldif_tmpfile="${10}"
   ___ul_ldapmodify_bin="${11}"
   ___ul_ldapmodify_flags="${12}"
   ___ul_test="${13}"
   ___ul_tmpfile1="${14}"

   # learn dn
   ___ul_dn="$( get_attrib_from_ldap "${___ul_ldapsearch_bin}" "${___ul_ldapsearch_flags}" "${___ul_ldapsearch_filter}" "${___ul_ldapsearch_unique_id}" "${___ul_ldapconf}" "${___ul_krb5cc_tmpfile}" "${___ul_tmpfile1}" )"

   # split attribs and values with delimiter
   ___ul_attrib_count=0
   ___ul_value_count=0
   # hope that the lowercase letter a is never used as a delimiter characater
   ___ul_attrib_string="$( echo "${___ul_attribs}" | tr "${___ul_delimiter:-|}" "\n" | sed -r -e '/^\s*$/d;' )"
   ___ul_value_string="$( echo "${___ul_values}" | tr "${___ul_delimiter:-|}" "\n" | sed -r -e '/^\s*$/d;' )"
   ___ul_attrib_count="$( echo "${___ul_attrib_string}" | wc -l )"
   ___ul_value_count="$( echo "${___ul_value_string}" | wc -l )"
   # if attrib count and value count mismatch, error out
   if test ${___ul_attrib_count} -ne ${___ul_value_count} ;
   then
      ferror "update_ldap: 1 fatal! Attribs and values mismatch: \"${___ul_attribs}\" and \"${___ul_values}\""
      exit 1
   fi

   # generate ldif
   {
      echo "${___ul_ldapsearch_unique_id}: ${___ul_dn}"
      echo "changetype: modify"
      # loop through attribs
      ___ul_x=0
      echo "${___ul_attrib_string}" | while read ___ul_ta ;
      do
         # print dash to separate replace statements that affect a single dn, but do not end the statements with a dash
         test ${___ul_x} -gt 0 && printf "%s\n" "-"
         ___ul_x=$(( ___ul_x + 1 ))
         ___ul_tv="$( echo "${___ul_value_string}" | sed -n -e "${___ul_x}p" )"
         echo "replace: ${___ul_ta}"
         echo "${___ul_ta}: ${___ul_tv}"
      done
   } > "${___ul_ldif_tmpfile}"
   unset ___ul_ldapmodify_flag_verbose ; debuglev 9 && ___ul_ldapmodify_flag_verbose="-v"

   # add -n to this command if flag --test is used.
   unset ___ul_ldapmodify_flag_test ; fistruthy "${___ul_test}" && ___ul_ldapmodify_flag_test="-n"
   {
      KRB5CCNAME="${___ul_krb5cc_tmpfile}" LDAPCONF="${___ul_ldapconf}" "${___ul_ldapmodify_bin}" ${___ul_ldapmodify_flags} "${___ul_ldif_tmpfile}" ${___ul_ldapmodify_flag_verbose} ${___ul_ldapmodify_flag_test} 2>&1 
      echo "$?" > "${___ul_tmpfile1}"
   }| sed -r -e '/^\s*$/d;' | debuglevoutput 1 silent
   ___ul_ldap_success="$( cat "${___ul_tmpfile1}" )"

   case "${___ul_ldap_success}" in
      0)
         # continue on
         :
         ;;
      *)
         ferror "update_ldap: 7 fatal! ldapmodify returned ${___ul_ldap_success}. Unhandled exception. Aborted."
         cat "${___ul_ldif_tmpfile}" | debuglevoutput 7 silent
         exit 7
         ;;
   esac

   return ${___ul_ldap_success}

}

get_user_kerberos_ticket() {
   # call: get_user_kerberos_ticket "${LAPS_KERBEROS_USER}" "${LAPS_USER_IS_ROOT}" "${LAPS_KRB5CC_TMPFILE}" "${LAPS_INTERACTIVE}" "${LAPS_KINIT_BIN}" "${LAPS_KLIST_BIN}"
   debuglev 10 && ferror "get_user_kerberos_ticket $@"
   ___gukt_kerberos_user="${1}"
   ___gukt_user_is_root="${2}"
   ___gukt_krb5cc_tmpfile="${3}"
   ___gukt_interactive="${4}"
   ___gukt_kinit_bin="${5}"
   ___gukt_klist_bin="${6}"

   # LAPS on the domain side does not permit a host keytab to read the password attribute, so if user=machine, fail out
   # options:
   # if root, using machine ticket. ACT: fail
   # if root, using user ticket.    ACT: check user tgt, then prompt.
   # if user, using machine ticket. ACT: check user tgt, then prompt
   # if user, using user ticket     ACT: check user tgt, then prompt

   if test "${___gukt_kerberos_user}" = "machine" ;
   then
      if test "${___gukt_user_is_root}" = "1" ;
      then
         ferror "get_user_kerberos_ticket: 2 fatal! To read the password stored in the domain, you need LDAP_KERBEROS_USER=<username> or -u <username> or run this script as a domain admin user. Aborted."
         exit 2
      else
         ___gukt_kerberos_user="${USER}"
         ferror "Trying with logged in user ${___gukt_kerberos_user}."
      fi
   fi

   # Try current user kerberos ticket to see if has a tgt for LAPS_KERBEROS_USER
   ___gukt_klist_stdout="$( "${___gukt_klist_bin}" 2>/dev/null )"
   echo "${___gukt_klist_stdout}" | debuglevoutput 8
   ___gukt_klist_krb5cc="$( echo "${___gukt_klist_stdout}" | grep -iE 'ticket cache:' | awk -F':' '{print $NF}' | xargs )"
   ___gukt_klist_user=$( echo "${___gukt_klist_stdout}" | grep -iE 'default principal:' | awk -F':' '{print $2}' | awk -F'@' '{print $1}' | xargs )
   ___gukt_klist_krbtgt="$( echo "${___gukt_klist_stdout}" | grep -E "krbtgt\/" )"
   {
      echo "klist_krb5cc=${___guktk_list_krb5cc}"
      echo "klist_user=${___gukt_klist_user}"
      echo "klist_krbtgt=${___gukt_klist_krbtgt}"
   } | debuglevoutput 7

   # if we already have a tgt
   if test -n "${___gukt_klist_krbtgt}" ;
   then
      case "${___gukt_klist_user}" in
         # and it is for the requested user
         ${___gukt_kerberos_user}) 
            # copy it to our temporary location
            debuglev 7 && ferror "Using existing krbtgt for requested user ${___gukt_kerberos_user}"
            /bin/cp -p "${___gukt_klist_krb5cc}" "${___gukt_krb5cc_tmpfile}"
            ;;
         *)
            ferror "Using existing krb5tgt for ${___gukt_klist_user} instead of requested ${___gukt_kerberos_user}"
            ___gukt_kerberos_user="${___gukt_klist_user}"
            ;;
      esac
   else
      # need to get a ticket
      # are we allowed to ormpt?
      if fistruthy "${___gukt_interactive}" ;
      then
         # prompt and save to temp kerberos location
         debuglev 1 && ferror "No krbtgt found. Prompting now..."
         KRB5CCNAME="${___gukt_krb5cc_tmpfile}" "${___gukt_kinit_bin}" "${___gukt_kerberos_user}"
      else
         ferror "get_user_kerberos_ticket: 2 fatal! Need SCRIPT_INTERACTIVE=1 or -i flag, to allow interactive kinit prompt. Aborted."
         exit 2
      fi
   fi
   # verify that the tgt exists now
   ___gukt_klist_stdout="$( KRB5CCNAME="${___gukt_krb5cc_tmpfile}" "${___gukt_klist_bin}" 2>/dev/null )"
   echo "${___gukt_klist_stdout}" | debuglevoutput 4
   ___gukt_klist_krb5cc="$( echo "${___gukt_klist_stdout}" | grep -iE 'ticket cache:' | awk -F':' '{print $NF}' | xargs )"
   ___gukt_klist_user=$( echo "${___gukt_klist_stdout}" | grep -iE 'default principal:' | awk -F':' '{print $2}' | awk -F'@' '{print $1}' | xargs )
   ___gukt_klist_krbtgt="$( echo "${___gukt_klist_stdout}" | grep -E "krbtgt\/" )"
   {
      echo "klist_krb5cc=${___gukt_klist_krb5cc}"
      echo "klist_user=${___gukt_klist_user}"
      echo "klist_krbtgt=${___gukt_klist_krbtgt}"
   } | debuglevoutput 3

   if test -z "${___gukt_klist_krbtgt}" ;
   then
      # no krbtgt so fail out
      ferror "get_user_kerberos_ticket: 6 fatal! Failed to get tgt for user ${___gukt_kerberos_user}. Check password or account. Aborted."
      exit 6
   fi

}
