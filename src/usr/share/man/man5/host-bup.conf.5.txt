title host-bup.conf
section 5
project bgscripts-core
volume File Formats Manual
date April 2020
=====
NAME
  host-bup.conf - configuration file for host-bup
SYNOPSIS
  host-bup [-d {0..10}] [-uV] [-c <conffile>]
DESCRIPTION
  The host-bup.conf is by default located at `/etc/installed/host-bup.conf` to fit the schema of the author. As long as you pass `-c <conffile>` to host-bup, it does not matter where it is.

  Two main sections exist. Lines beginning with a pound `#` symbol are treated as comments.

HOSTBUP MAIN SECTION
Heading is `[hostbup:main]`.  
  This section is very similar to a shell script that gets sourced. Each entry consists of:

  name=value

With no spaces around the equal sign.
* hostname  must be exact for this host name. This is a safety check in case the host-bup.conf files are centrally stored and the wrong one is selected. This can be ignored by passing `-f` to host-bup.
* tar_out_file  Destination tarball. Can include parameter expansion including subshell execution as seen in the EXAMPLE file.
* script_count  How many script_X_cmd entries exist.
* script_1_cmd  This whole line will be invoked in a shell, so redirection is allowed.
* dryrun  If set to 1, take no action.
HOSTBUP FILES SECTION
Heading is `[hostbup:files]`.  
  One full path file per line. Does not support directories.
EXAMPLES
  See `/usr/share/bgscripts/examples/host-bup.conf.example`
AUTHOR
  <bgstack15@gmail.com>
COPYRIGHT
  CC-BY-SA 4.0
SEE ALSO
`host-bup(1)`
