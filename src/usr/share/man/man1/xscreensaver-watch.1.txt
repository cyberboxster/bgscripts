title xscreensaver-watch
section 1
project bgscripts
date April 2020
volume General Commands Manual
=====
NAME
  xscreensaver-watch - trigger actions based on xscreensaver status
SYNOPSIS
  xscreensaver-watch
ENVIRONMENT
  xscreensaver-watch loads /etc/default/xscreensaver-watch before executing anything else.
  XSW_LOGFILE
  XSW_PATH  Colon-separated just like $PATH. Will be prepended to $PATH, which affects resolution order for the `xsw-on-unblanked` and similar ACTIONS.
DESCRIPTION
  xscreensaver-watch is a user daemon that listens to the xscreensaver daemon. When xscreensaver emits messages like `LOCKED` or `UNBLANKED`, this script will invoke commands as described in ACTIONS.

  This is useful for when you want to always run a command when the screen is locked or unlocked.

  The user must invoke `xscreensaver-watch` at startup time. The bgscripts package already placed this in /etc/xdg/autostart/xscreensaver-watch.desktop.
  For `fluxbox(1)` users, add this to your ${HOME}/.fluxbox/startup:
  XSW_PATH="${HOME}" /usr/bin/xscreensaver-watch &
ACTIONS
  When xscreensaver emits LOCKED, invoke command xsw-on-locked.
  When xscreensaver emits UNBLANKED, invoke command xsw-on-unblanked.
AUTHOR
  <bgstack15@gmail.com>
COPYRIGHT
  CC-BY-SA 4.0
BUGS
* Not all xscreensaver emissions are used.
* Not easy to restart.
SEE ALSO
`xscreensaver-command(1)`
