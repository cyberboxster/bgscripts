title make-dsc-for-obs
section 1
project bgscripts-core
volume General Commands Manual
date April 2020
=====
NAME
  make-dsc-for-obs - make debian package dsc file for open build service
SYNOPSIS
  APPNAME=<name> make-dsc-for-obs
ENVIRONMENT
APPNAME=<name>  
  Most of the dpkg build instructions by the author already define APPNAME in debian/rules.
DESCRIPTION
Run this from the source directory, where `debian/` also exists.

This script strips the checksums from the dsc file, and places it in the `debian/` directory so it can be saved to the source tree on the Internet. The Open Build Service <https://build.opensuse.org/> then can pull this file which provides the necessary instructions to initiate a build of a Debian package.

The idea is to build the package locally, to make sure it compiles. This task runs, to copy in the updated .dsc file to the source tree, so that the OBS can then use it later.
CALLED BY DEBIAN RULES FILE
Use this script in a `debian/rules` file like so.

    APPNAME=freefilesync

    override_dh_auto_build:
       dh_auto_build
       make-dsc-for-obs
BUGS
  Not completely tested, but does work for multiple packages for the author.
AUTHOR
  <bgstack15@gmail.com>
COPYRIGHT
  CC-BY-SA 4.0
SEE ALSO
`debuild(1)`, `debhelper(7)`
