title kinit-host
section 1
project bgscripts-core
volume General Commands Manual
date April 2020
=====
NAME
  kinit-host - obtain host Kerberos ticket-granting ticket from host keytab
SYNOPSIS
  kinit-host [-d {0..10}] [-uV] [-c <conffile>] [-u|-l] [-D|-n] [-f|-s] [--hostname <HOSTNAME>] [--domain <DOMAIN>] [--atdomain <DOMAIN>] [-q] [--kinit </path/to/kinit>] [-N]
OPTIONS
  -d {0..10}    Set debug level, with 10 as maximum verbosity.
  -u --usage    Show help message
  -V --version  Show version number
  -c <conffile> Use this config file in addition to global default of `/etc/default/kinit-host` and user default of `${HOME}/.config/kinit-host`
  -u --upper     Set KH_UPPER=1
  -l --lower     Set KH_UPPER=0
  -D --dollar    Set KH_DOLLAR=1
  -n --no-dollar Set KH_DOLLAR=0
  -f --fqdn      Set KH_FQDN=1
  -s --short     Set KH_FQDN=0
  --hostname  <value> Set KH_HOSTNAME=<value>
  --domain    <value> Set KH_DOMAIN=<value>
  --atdomain  <value> Set KH_ATDOMAIN=<value>
  -q --silent    Set KH_SILENT=1
  --kinit     <path>  Set path to `kinit(1)`
  -N --dry-run   Set KH_DRYRUN=1
ENVIRONMENT
kinit-host is managed mostly by environment variables. The parameters manipulate these variables. The parameters will take precedence over any established environment variables or config file options (also loaded as environment variables).
* KH_UPPER={0,1} Use (0) lowercase or (1) uppercase name in request. Default is 1.
* KH_FQDN={0,1} Use (0) shortname or (1) fqdn. Default is 0.
* KH_DOLLAR={0,1} Append `$` dollar sign to end. Default is 1.
* KH_KRB5_CONF Use this krb5.conf file. Default is `/etc/krb5.conf`.
* KH_KINIT Use this path to `kinit(1)` binary. Default is `/usr/bin/kinit`.
* KH_DOMAIN Use this domain for KH_FQDN action. Default is derived from KH_KRB5_CONF file.
* KH_ATDOMAIN Use this domain in the request after the `@` at symbol. Defaults to KH_DOMAIN.
* KH_SILENT={0,1} Suppress `kinit(1)` errors. Default is 0.
* KH_DRYRUN={0,1} Suppress action. Default is 0.
DESCRIPTION
  Kinit-host is designed to facilitate fetching a Kerberos keytab for the host, in an Active Directory domain. It can be adapted for other Kerberos domains, but the defaults are definitely configured to make it easy to test host keytab correctness in an AD environment.
NOTEWORTHY DEBUG LEVELS
* 1 kinit request
* 5 all KH_ environment parameters
BUGS
  Focuses on Active Directory, when open source networks iwill not use this technology.
AUTHOR
  <bgstack15@gmail.com>
COPYRIGHT
  CC-BY-SA 4.0
SEE ALSO
  `kinit(1)`, `krb5.conf(5)`, `kinit-host(5)`
