#!/bin/sh

BUILD_VERSION=2018-11-25

##### begin snippet from framework
thisos="$( uname -s )"
# get thisflavor and thisflavorversion. Examples: centos, ubuntu, redhat
if test -f /etc/os-release;
then
   eval thisflavor=$( grep -iE "^\s*ID=" /etc/os-release 2>/dev/null | sed 's/^.*=//;' | tr 'A-Z' 'a-z' )
   eval thisflavorversion=$( grep -iE "^\s*PRETTY_NAME=" /etc/os-release 2>/dev/null | sed -e 's/^.*=//;' | tr -dc '0-9.' )
elif test -f /etc/system-release && test $( wc -l < /etc/system-release 2>/dev/null ) -eq 1;
then
   eval thisflavor=$( awk '{print $1}' < /etc/system-release 2>/dev/null | tr 'A-Z' 'a-z' )
   eval thisflavorversion=$( </etc/system-release sed -e 's/^.*=//;' 2>/dev/null | tr -dc '0-9.' )
else
   if test "${thisos}" = "FreeBSD"; then
      thisflavor="$( uname -i )"; thisflavorversion="$( uname -r )";
   else
      thisflavor="other"
      thisflavorversion="unknown"
   fi
fi
##### end snippet from framework

echo " $@ " | grep -qiE -- "help|usage|\s-h\s|\s-\?" 1>/dev/null 2>&1 && {
   # display help and exit
   less -F <<EOF
build utility version ${BUILD_VERSION}
usage: pack [ rpm | deb | tar ] [ --debug | -d {0-10} ]
Provides a single command for building a package. This script is customized to each package.
optional arguments:
 [ rpm | deb | tar | bsd ]    Build that type of package. 
                              The default depends on the local os flavor. This system is "${thisflavor}"
 [ --debug {0-10} | -d {0-10} ] Display package type to build and exit. Debuglev not implemented here.
EOF
   exit 1
}

# Initialize variables to use
test -z "${PACK_PACKAGE}" && PACK_PACKAGE="$( basename "$( dirname "$( readlink -f "$0" )" )" | sed -r -e 's/-[0-9\-\.]+$//;' )" 
test -z "${PACK_DIR}" && PACK_DIR="$( readlink -f "$( dirname "${0}" )" )"
test -z "${PACK_VERSION}" && PACK_VERSION="$( cat "${PACK_DIR}/src/usr/share/doc/${PACK_PACKAGE}/version.txt" )"
test -z "${RPMBUILD_SOURCES_DIR}" && RPMBUILD_SOURCES_DIR="/home/${USER}/rpmbuild/SOURCES"
test -z "${DEB_DIR}" && DEB_DIR="/home/${USER}/deb"

test -z "${PACK_TYPE}" && PACK_TYPE="${1}"

# set default value based on flavor of Linux
if test -z "${PACK_TYPE}" ;
then
   case "${thisflavor}" in
      redhat|rhel|centos|fedora|korora) PACK_TYPE=rpm ;;
      debian|ubuntu|devuan) PACK_TYPE=deb ;;
      GENERIC) PACK_TYPE=freebsd ;;
      *) PACK_TYPE=targz ;;
   esac
fi

# allow command line parameters to override any flavor defaults
echo " $@ " | grep -qiE -- "rpm|rhel|redhat|centos|fedora|korora" 1>/dev/null 2>&1 && PACK_TYPE=rpm
echo " $@ " | grep -qiE -- "debian|ubuntu|deb\s|dpkg" 1>/dev/null 2>&1 && PACK_TYPE=deb
echo " $@ " | grep -qiE -- "freebsd|\bbsd\b" 1>/dev/null 2>&1 && PACK_TYPE=freebsd
echo " $@ " | grep -qiE -- "tar|tgz|gz" 1>/dev/null 2>&1 && PACK_TYPE=targz

# prepare to build tarball
case "${PACK_TYPE}" in

   rpm|targz)
      SOURCES_DIR="${RPMBUILD_SOURCES_DIR}"
      # for rpm, ensure dir name is ${name}
      if test "$( basename "${PACK_DIR}" )" != "${PACK_PACKAGE}" ;
      then
         if ! test -e "${PACK_DIR}/../${PACK_PACKAGE}"
         then
            ln -s "$( basename "${PACK_DIR}" )" "${SOURCES_DIR}/${PACK_PACKAGE}"
            PACK_DIR="$( readlink -f "${SOURCES_DIR}/${PACK_PACKAGE}" )"
         fi
      fi
      ;;

   deb)
      SOURCES_DIR="${DEB_DIR}"
      # for deb, ensure dir name is ${name}-${version}
      if test "$( basename "${PACK_DIR}" )" != "${PACK_PACKAGE}-${PACK_VERSION}" ;
      then
         if ! test -e "${PACK_DIR}/../${PACK_PACKAGE}-${PACK_VERSION}"
         then
            ln -s "$( basename "${PACK_DIR}" )" "${SOURCES_DIR}/${PACK_PACKAGE}-${PACK_VERSION}"
            PACK_DIR="$( readlink -f "${SOURCES_DIR}/${PACK_PACKAGE}-${PACK_VERSION}" )"
         fi
      fi
      ;;

   *)
      echo "No initialization available for unknown PACK_TYPE ${PACK_TYPE}."
      ;;

esac

___oldpwd="$( pwd )" ; cd "${PACK_DIR}/.."
/bin/rm -f "${PACK_PACKAGE}-${PACK_VERSION}.tar.gz" "${PACK_PACKAGE}_${PACK_VERSION}.orig.tar.gz"
tar -zcf "${SOURCES_DIR}/${PACK_PACKAGE}-${PACK_VERSION}.tgz" --exclude=".git" --exclude=".*.swp" "$( basename "${PACK_DIR}" )"
cd "${___oldpwd}"

case "${PACK_TYPE}" in

   rpm)
      ___oldpwd="$( pwd )" ; cd "${PACK_DIR}"
      rpmbuild -ba "${PACK_PACKAGE}.spec"
      cd "${___oldpwd}"
      ;;

   deb)
      #debmake # was only needed the very first time and then hand-curated ever since.
      ___oldpwd="$( pwd )" ; cd "${PACK_DIR}"
      ln -s "${PACK_PACKAGE}-${PACK_VERSION}.tgz" "../${PACK_PACKAGE}_${PACK_VERSION}.orig.tar.gz"
      debuild -us -uc -nc -tc
      debuild -- clean 1>/dev/null 2>&1
      cd "${___oldpwd}"
      ;;

   targz)
      # nothing to do here because tarball was already made
      :
      ;;

   *)
      echo "Unknown type ${PACK_TYPE}"
      exit 1
      ;;

esac
